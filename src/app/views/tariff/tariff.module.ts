import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {TariffRoutingModule} from './tariff-routing.module';
import {PackageComponent} from './package/package.component';
import {InternetComponent} from './internet/internet.component';
import {TvComponent} from './tv/tv.component';
import {PhoneComponent} from './phone/phone.component';
import {IntercomComponent} from './intercom/intercom.component';

import {TabsModule} from 'ngx-bootstrap/tabs';

import {ModalModule} from 'ngx-bootstrap/modal';

@NgModule({
    declarations: [PackageComponent, InternetComponent, TvComponent, PhoneComponent, IntercomComponent],
    imports: [
        CommonModule,
        TariffRoutingModule,
        TabsModule,
        ModalModule.forRoot()
    ],
    exports: [PackageComponent, InternetComponent, TvComponent, PhoneComponent, IntercomComponent],
    entryComponents: [PackageComponent, InternetComponent, TvComponent, PhoneComponent, IntercomComponent]
})
export class TariffModule {
}
