import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';

import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';

import { InternetTariff } from '../models/internet-tariff';
import {ApiResponse} from '../models/api-response';


@Injectable({
  providedIn: 'root'
})
export class InternetTariffService {
  tariffsUrl = 'https://master.api-content.web.t2.ertelecom.ru/perm/v1/internet-tariffs?per-page=50';  // URL to web api
  tariffUrl = 'https://master.api-content.web.t2.ertelecom.ru/perm/v1/internet-tariffs/';
  // private handleError: HandleError;

  constructor(private http: HttpClient,
              /*httpErrorHandler: HttpErrorHandler*/) {
    // this.handleError = httpErrorHandler.createHandleError('InternetTariffService');
  }

  getTariffs (): Observable<ApiResponse> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer 8277e0910d750195b448797616e091ad'
      })
    };
    return this.http.get<ApiResponse>(this.tariffsUrl, httpOptions)
      .pipe(
        // catchError(this.handleError('getTariffs', []))
      );
  }

  getTariff (id): Observable<InternetTariff> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': 'Bearer 8277e0910d750195b448797616e091ad'
      })
    };
    return this.http.get<InternetTariff>(this.tariffUrl + id, httpOptions);
  }

  updateTariff (tariff: InternetTariff): Observable<InternetTariff> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
        'Authorization': '4a8a08f09d37b73795649038408b5f33'
      })
    };

    return this.http.put<InternetTariff>(this.tariffUrl, tariff, httpOptions)
      .pipe(
        // catchError(this.handleError('updateTariff', tariff))
      );
  }
}
