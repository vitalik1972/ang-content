import { TestBed } from '@angular/core/testing';

import { InternetTariffService } from './internet-tariff.service';

describe('InternetTariffService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternetTariffService = TestBed.get(InternetTariffService);
    expect(service).toBeTruthy();
  });
});
