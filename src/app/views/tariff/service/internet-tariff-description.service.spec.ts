import { TestBed } from '@angular/core/testing';

import { InternetTariffDescriptionService } from './internet-tariff-description.service';

describe('InternetTariffDescriptionService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: InternetTariffDescriptionService = TestBed.get(InternetTariffDescriptionService);
    expect(service).toBeTruthy();
  });
});
