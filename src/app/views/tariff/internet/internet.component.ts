import {Component, OnInit, ViewChild} from '@angular/core';

import {InternetTariff} from '../models/internet-tariff';
import {InternetTariffService} from '../service/internet-tariff.service';
import {ApiResponse} from '../models/api-response';


@Component({
    selector: 'app-internet',
    templateUrl: './internet.component.html',
    styleUrls: ['./internet.component.scss']
})
export class InternetComponent implements OnInit {

    tariffs: {
        items: InternetTariff[],
        _links: {},
        _meta: {}
    };

    tariff: InternetTariff;

    @ViewChild('viewModal') viewModal;
    @ViewChild('updateModal') updateModal;

    constructor(private internetTariffService: InternetTariffService) {
    }

    ngOnInit() {
        this.getTariffs();
    }

    getTariffs() {
        this.internetTariffService.getTariffs()
            .subscribe((tariffs: ApiResponse) => {
                this.tariffs = tariffs
            });
    }

    log() {
        console.log(this.tariffs.items);
    }

    getTariff(id) {
        this.internetTariffService.getTariff(id)
            .subscribe((tariff: InternetTariff) => this.tariff = tariff);
    }
    showTariff(id) {
        console.log('show ' + id);
        this.getTariff(id);
        this.viewModal.show();
    }

    updateTariff(id) {
        console.log('update ' + id);
        this.getTariff(id);
        this.updateModal.show();
    }
}
