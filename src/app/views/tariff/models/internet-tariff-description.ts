export class InternetTariffDescription {
  id: number;
  tariff_id: number;
  city_id: number;
  billing_city_id: number;
  tariff_name: string;
  description: string;
  oe_client_available: number;
  oe_client_icon: string;
  oe_client_banner_text: string;
}
