import {InternetTariff} from './internet-tariff';

export class ApiResponse {
    items: InternetTariff[];
    _links: {};
    _meta: {};
}
