import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {PackageComponent} from './package/package.component';
import {InternetComponent} from './internet/internet.component';
import {TvComponent} from './tv/tv.component';
import {PhoneComponent} from './phone/phone.component';
import {IntercomComponent} from './intercom/intercom.component';

const routes: Routes = [
  {
    path: '',
    data: {
      title: 'Тарифы'
    },
    children: [
      {
        path: '',
        redirectTo: 'package'
      },
      {
        path: 'package',
        component: PackageComponent,
        data: {
          title: 'Пакеты'
        }
      },
      {
        path: 'internet',
        component: InternetComponent,
        data: {
          title: 'Тарифы моно-интернеты'
        }
      },
      {
        path: 'tv',
        component: TvComponent,
        data: {
          title: 'Тарифы моно-тв'
        }
      },
      {
        path: 'phone',
        component: PhoneComponent,
        data: {
          title: 'Тарифы телефонии'
        }
      },
      {
        path: 'intercom',
        component: IntercomComponent,
        data: {
          title: 'Тарифы домофонии'
        }
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TariffRoutingModule {
}
