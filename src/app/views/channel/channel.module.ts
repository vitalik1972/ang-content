import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChannelRoutingModule } from './channel-routing.module';
import { ChannelComponent } from './channel.component';

@NgModule({
  declarations: [ChannelComponent],
  imports: [
    CommonModule,
    ChannelRoutingModule
  ],
  exports: [ChannelComponent],
  entryComponents: [ChannelComponent]
})
export class ChannelModule { }
