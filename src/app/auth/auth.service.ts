import { Injectable } from '@angular/core';

/** Mock client-side authentication/authorization service */
@Injectable()
export class AuthService {
    getAuthorizationToken() {
        return 'Bearer 8277e0910d750195b448797616e091ad';
    }
}
